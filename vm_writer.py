"""
vm_writer defines the VMWriter class. Thsi class provides methods for writing
VM code to .vm files corresponding to the .jack files being compiled
"""
import os

class VMWriter():
    #creates and prepares VM file
    def __init__(self, vm_file, dir):
        self.line_num = -1
        self.trackers = []
        self.file_id = dir + vm_file + ".vm"
        if os.path.exists(self.file_id):
            os.remove(self.file_id)
        self.file = open(self.file_id ,"a+")

    #writes a VM push command
    def write_push(self, segment, index):
        self.line_num += 1
        self.file.write("   push " + segment.lower() + " " + str(index) + '\n')

    #writes a VM pop command
    def write_pop(self, segment, index):
        self.line_num += 1
        self.file.write("   pop " + segment.lower() + " " + str(index) + '\n')

    #writes a VM arithmetic command
    def write_arith(self, command):
        self.line_num += 1
        if command not in ["add", "sub", "neg", "eq", "gt", "lt", "and", "or", "not"]:
            print("Error: nonexistant command given")
            exit(0)

        self.file.write("   " + command + "\n")

    #writes a VM label
    def write_label(self, string):
        self.line_num += 1
        string_split = string.split(' ')
        if len(string_split) > 1:
            print("Error: string too long")
        elif string == "":
            print("Error: string empty")

        self.file.write("label " + string + '\n')

    #writes a VM goto command
    def write_goto(self, string):
        self.line_num += 1
        string_split = string.split(' ')
        if len(string_split) > 1:
            print("Error: string too long")
        elif string == "":
            print("Error: string empty")

        self.file.write("   goto " + string + '\n')

    #writes a VM if-goto command
    def write_if(self, string):
        self.line_num += 1
        string_split = string.split(' ')
        if len(string_split) > 1:
            print("Error: string too long")
        elif string == "":
            print("Error: string empty")

        self.file.write("   if-goto " + string + '\n')

    #writes a VM call command
    def write_call(self, name, n_args):
        self.line_num += 1
        self.file.write("   call " + name + " " + str(n_args) + '\n')

    #writes a VM function command
    def write_func(self, name, n_locals):
        self.line_num += 1

        self.file.write("function " + name + " " + str(n_locals) + "\n")

    #writes a VM return command
    def write_ret(self):
        self.line_num += 1
        self.file.write("   return" + '\n')

    """
    Adds a NArgTracker that keeps track of lines that need to be updated once
    parsing of the files is complete.
    """
    def add_narg_tracker(self, class_name, method_id):
        #add temporary line
        self.write_call("correct_later", 0)
        self.trackers.append(NArgTracker(self.line_num, class_name, method_id))

    """
    correct_lines reads the lines written in parsing the jack file into a list
    the list of NArgTrackers is then iterated through and lines pushing the number
    of function arguments are amended. The .vm file is then overwritten by the
    corrected file.
    """
    def correct_lines(self, tables):
        self.close()
        file = open(self.file_id, "r")
        lines = file.readlines()
        file.close()
        for tracker in self.trackers:
            tracker.correct(tables, lines)
        os.remove(self.file_id)
        file = open(self.file_id, 'w')
        for line in lines:
            file.write(line)
        file.close()

    #closes the VM file
    def close(self):
        self.file.close()

"""
NArgTracker defines a class used to keep track of lines where the number of function
arguments was not known at the time, so that they can be corrected after parsing
all of the files.
"""
class NArgTracker():
    def __init__(self, line_num, class_name, method_id):
        self.line_num = line_num
        self.class_name = class_name
        self.method_id = method_id

    #correct is the method that corrects the .vm line in the list of lines
    def correct(self, tables, lines):
        class_table = None
        for table in tables:
            if table.class_name == self.class_name:
                class_table = table
                break;

        n_args = class_table.get_sub_no_args(self.method_id)
        lines[self.line_num] = (
                                "   call " + self.class_name + "."
                                + self.method_id + " " + str(n_args) + "\n"
        )
