"""
Defines a symbol table to be used for a class
"""
import parser as ps

class Symbol:
    type = ""
    kind = ""
    address = -1
    initialised = False

    def __init__(self, type, kind, address):
        self.type = type
        self.kind = kind
        self.address = address


class SymbolTable:

    static_index, field_index, arg_index, var_index = -1, -1, -1, -1

    def __init__(self):
        self.class_name = ""
        self.class_table = {}
        self.sub_tables = {}
        self.sub_table = {}

    #starts a new symbol table for a sub procedure and adds the previous one to sub_tables
    def new_sub(self, sub_ident, sub_kind, sub_type):
        self.sub_table = {}
        self.sub_tables[sub_ident] = self.sub_table
        self.arg_index, self.var_index = -1, -1

        self.sub_table["return"] = Symbol(sub_type, sub_kind, -1)

        if sub_kind == "method":
            self.insert("this", self.class_name, "arg")
        else:
            self.arg_index = -1

    #inserts an identifier into the symbol table (class level or procedure) with attributes
    def insert(self, name, type, kind):
        counter = 0
        if kind == "field" or kind == "static":
            if kind == "field":
                self.field_index += 1
                counter = self.field_index

            if kind == "static":
                self.static_index += 1
                counter = self.static_index

            new_symbol = Symbol(type, kind, counter)

            self.class_table[name] = new_symbol

        if kind == "arg" or kind == "var":
            if kind == "arg":
                self.arg_index += 1
                counter = self.arg_index

            if kind == "var":
                self.var_index += 1
                counter = self.var_index

            new_symbol = Symbol(type, kind, counter)

            if new_symbol.kind == "arg":
                new_symbol.initialised = True

            self.sub_table[name] = new_symbol

    #compares a given name against variable identifiers of all scopes within
    #the class table returns true if identifier is found
    def look_up(self, name):
        if name in ps.class_names:
            return True

        if self.sub_table.get(name):
            return True

        for identifier, sub_table in self.sub_tables.items():
            if sub_table.get(name):
                return True

        if self.class_table.get(name):
            return True

        if self.sub_tables.get(name):
            return True

        return False

    #check whether sub procedure with given name exist in the class table
    def look_up_sub(self, name):
        sub = self.sub_tables.get(name)
        if sub:
            return True


    #looks up a given name and returns the type attribute of the symbol
    def look_up_var_type(self, name):
        symbol = self.sub_table.get(name)
        if symbol:
            return symbol.type

        for identifier, sub_table in self.sub_tables.items():
            symbol = sub_table.get(name)
            if symbol:
                return symbol.type

        symbol = self.class_table.get(name)
        if symbol:
            return symbol.type


    #sets the initialised variable of the Symbol (given by name)
    def set_var_initialised(self, name):
        symbol = self.sub_table.get(name)
        if symbol:
            symbol.initialised = True

        for identifier, sub_table in self.sub_tables.items():
            symbol = sub_table.get(name)
            if symbol:
                symbol.initialised = True

        symbol = self.class_table.get(name)
        if symbol:
            symbol.initialised = True

    #checks whether the symbol with the given name has been initialised
    def check_initialised(self, name):
        symbol = self.sub_table.get(name)
        if symbol:
            if symbol.initialised:
                return True

        for identifier, sub_table in self.sub_tables.items():
            symbol = sub_table.get(name)
            if symbol:
                if symbol.initialised:
                    return True

        symbol = self.class_table.get(name)
        if symbol:
            if symbol.initialised:
                return True

        return False

    #gets the number of arguments that a specified sub procedure expects
    def get_sub_no_args(self, name):
        no_args = 0
        sub = self.sub_tables.get(name)
        if sub:
            for id, symbol in sub.items():
                if symbol.kind == "arg":
                    no_args += 1
        return no_args


    #checks whether a given name can be used in the current sub procedure scope
    #returns false if the identifier is not in use
    def check_used_sub(self, name):
        if name in ps.class_names:
            return True

        for identifier, sub_table in self.sub_table.items():
            if identifier == name:
                return True
        for identifier, val in self.class_table.items():
            if identifier == name:
                return True

        return False

    #returns the return type of a specified sub procedure
    def sub_ret_type(self, name):
        return_type = self.sub_tables.get(name)
        if return_type:
            return return_type["return"].type

    #returns the kind of a specified sub procedure
    def sub_kind(self, name):
        sub = self.sub_tables.get(name)
        if sub:
            return sub["return"].kind

    #returns the type sequence of a sub procedures arguments as a list
    #i.e. integer(int abc, char edc) returns ['int', 'char']
    def get_arg_type_sequence(self, name):
        sub = self.sub_tables.get(name)
        sequence = []
        if self.sub_kind(name) == "function" or self.sub_kind(name) == "constructor":
            start_address = -1
        else:
            start_address = 0

        if sub:
            for id, symbol in sub.items():
                if symbol.kind == "arg" and symbol.address > start_address:
                    sequence.append(symbol.type)
        return sequence

    #returns the address of the given symbol used to calculate memory offset
    def get_var_offset(self, name):
        symbol = self.sub_table.get(name)
        if symbol:
            return symbol.address

        for identifier, sub_table in self.sub_tables.items():
            symbol = sub_table.get(name)
            if symbol:
                return symbol.address

        symbol = self.class_table.get(name)
        if symbol:
            return symbol.address

    #returns the segment of memory in which the variable resides
    def get_var_segment(self, name):
        symbol = self.sub_table.get(name)
        if symbol:
            if symbol.kind == "var":
                return "local"
            else:
                return "argument"

        symbol = self.class_table.get(name)
        if symbol:
            if symbol.kind == "static":
                return "static"
            else:
                return "this"

    #returns the number of field variables in the class table
    #used to find the amount of memory to allocate
    def get_n_field_vars(self):
        count = 0
        for symbol_id, vals in self.class_table.items():
            if vals.kind == "field":
                count += 1
        return count

    #prints the symbol table in an easy to read format
    def symbol_table_to_string(self):
        print("\n----Class Symbol Table----")
        for key, val in self.class_table.items():
            print(key, "---->", "type:", val.type, "kind:", val.kind, "index:", val.address, "init:", val.initialised)
        for name, sub_table in self.sub_tables.items():
            print("\n====Method " + name + " Symbol Table====")
            for key, val in sub_table.items():
                print(key, "---->", "type:", val.type, "kind:", val.kind, "index:", val.address)
