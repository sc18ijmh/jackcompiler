"""
lexer defines a class implementation for Lexer and Token. The Lexer class
provides the functionality to tokenise the provided file, while also giving
an interface through which to access the tokens via peek/get_next_token().
"""

"""
Token is a class representation of a token. Holds data about a token.
"""
class Token:
    def __init__(self, lex, type, line_num):
        self.lexeme = lex
        self.type = type
        self.line_num = line_num

"""
Lexer is a class implementation of the lexical analysis phase of compilation.
Lexer takes a file path and tokenises the words within that file.
"""
class Lexer():
    reservedWords = [
        "class", "constructor", "method", "function", "int",
        "boolean", "char", "void", "var", "static",
        "field", "let", "do", "if", "else", "while", "return",
        "this"
    ]
    symbols = [
        "{", "}", "[", "]", "(", ")", ",", ".", ";", "=", "+", "*", "/",
        "&", "|", "~", "<", ">", "!", "-"
    ]

    def __init__(self, jack_file = None, dir = None):
        self.line_num = 0
        self.tokens = []
        self.comment_token_buffer = []
        #if a .jack files has been passed read the lines into a list
        if jack_file:
            file = open(jack_file, 'r')
            lines = file.readlines()
            lines.append("EOF")
            file.close()
            self.tokenise(lines)
            components = jack_file.split('/')
            self.filename = components[-1][:-5]
            self.dir = dir

    """
    get_line() returns the next line in the list of lines. The line to be returned
    is found using the self.line_num attribute.

    Strips leading and trailing whitespace from the line.
    """
    def get_line(self, lines):
        line = lines[self.line_num]
        line = line.strip()
        self.line_num += 1
        return line

    """
    tokenise_word() processes the word to determine its type. Once determined
    a Token object is instantiated for that word and appended to self.tokens.
    """
    def tokenise_word(self, word):
        #checks if the word is a keyword
        if ''.join(word) in self.reservedWords:
            self.tokens.append(Token(''.join(word),"keyword", self.line_num))
        else:
            #checks whether the word is an integer constant
            if word[0].isdigit() == True:
                for char in word:
                    if char.isdigit() == False:
                        print(
                                "Error: near '" + char + "' on line "
                                + str(self.line_num)
                                + ", letter in integer constant"
                        )
                        exit(0)
                self.tokens.append(Token(''.join(word), "constant", self.line_num))
            #checks if the word is boolean or null constant)
            elif ''.join(word) == "null" or ''.join(word) == "true" or ''.join(word) == "false":
                self.tokens.append(Token(''.join(word), "constant", self.line_num))
            #checks if the word is an identifier
            elif word[0].isalpha() == True:
                for char in word:
                    if char.isdigit() == False and char.isalpha() == False and char != "_":
                        print(
                                "Error: near '" + char + "' on line "
                                + str(self.line_num)
                                + " invalid character in identifier"
                        )
                        exit(0)
                self.tokens.append(Token(''.join(word), "identifier", self.line_num))
            #checks if the word is a string constant
            elif word[0] == '"' and word[len(word)-1] == '"':
                self.tokens.append(Token(''.join(word), "constant", self.line_num))


    """
    handle_string() checks if the word is a string constant (in which
    case control is passed back to tokenise()) until the closing '"' is found, in
    which case the word is tokenised.
    """
    def handle_string(self, word, char, string):
        if char == '"' and string == False:
            return True
        elif char == '"' and string == True:
            self.tokenise_word(word)
            word.clear()
            return False
        else:
            return string


    """
    tokenise() iterates over each line read from the provided file. When a complete
    word is found (i.e. a stream of character surrounded by whitespace or an operator
    symbol) the word is tokenised and added to self.tokens.

    While iterating if a comment is found then all chars are ignored until the
    comment has ended.
    """
    def tokenise(self, lines):
        block_comment = False
        pos_comment = False
        string = False
        eof_reached = False

        while not eof_reached:
            line = self.get_line(lines)
            if string == True:
                print(
                    "Error: near '" + ''.join(word) + "' on line "
                    + str(self.line_num) + ", newline found in string constant"
                )
                exit(0)
            word = []
            # Isolate word
            for char in line:
                # if char is not whitespace or a symbol then continue forming word
                if (
                    char != " "
                    and char not in self.symbols
                    and block_comment == False
                    or string == True
                ):
                    word.append(char)
                    if ''.join(word) == "EOF":
                        eof_reached = True
                    string = self.handle_string(word, char, string)
                # set pos_comment to true if '/' found for potential comment
                elif char == "/":
                    #may still be a word to tokenise
                    if len(word) > 0:
                        string = False
                        self.tokenise_word(word)
                        word = []

                    # if block_comment is true and the '*' token has been added
                    #to the buffer then the block comment has ended
                    if (
                        block_comment == True
                        and len(self.comment_token_buffer) > 0
                    ):
                        block_comment = False
                        self.comment_token_buffer.clear()

                    elif (
                            block_comment == True
                            and len(self.comment_token_buffer) == 0
                    ):
                        pass
                    else:
                        # add '/' token to buffer so if not comment the symbol
                        #is written to token list
                        self.comment_token_buffer.append(
                            Token(char, "symbol",
                            self.line_num)
                        )

                        if pos_comment == False:
                            pos_comment = True
                        else:
                            self.comment_token_buffer.clear()
                            break;

                # if pos_comment is true and char == '*' then a
                # block comment has been opened
                elif char == "*" and pos_comment == True:
                    block_comment = True
                    pos_comment = False
                    self.comment_token_buffer.clear()

                # if block_comment is true and  '*' we may find the end of
                #the block comment
                elif char == '*' and block_comment == True:
                    self.comment_token_buffer.append(Token(char, "symbol", self.line_num))

                # end of word has been found
                else:
                    # if pos_comment is true then there is a '/' token in the
                    #comment_token_buffer to add to tokens
                    if pos_comment:
                        pos_comment = False
                        for x in self.comment_token_buffer:
                            self.tokens.append(x)
                        self.comment_token_buffer.clear();

                    # if word is not empty then tokenise it
                    if len(word) > 0:
                        self.tokenise_word(word)
                        word = []

                    # if char is a symbol add it to tokens
                    if char in self.symbols and block_comment is False:
                        self.tokens.append(Token(char, "symbol", self.line_num))

                    #if the comment_token_buffer has an item in it then clear it
                    if len(self.comment_token_buffer) > 0:
                        self.comment_token_buffer.clear()


        #if all lines of file have ben consumed and block comment is still
        #open then the comment is not closed
        if block_comment == True:
            print(
                    "Error: on line " + self.line_num
                    + "end of file found within unclosed block comment on line "
            )
            exit(0)
        elif string == True:
            print(
                    "Error: on line " + self.line_num
                    + "end of file found within unclosed string on line "
            )
            exit(0)

    """
    get_next_token() returns and consumes the first token in self.tokens. Returns
    None if the list is empty
    """
    def get_next_token(self):
        if self.tokens:
            return self.tokens.pop(0)
        else:
            return None

    """
    peek_next_token() returns the first token in the list of tokens or None
    if self.tokens is empty
    """
    def peek_next_token(self):
        if self.tokens:
            return self.tokens[0]
        else:
            return None

    """
    get_tokens() returns the list of tokens.
    """
    def get_tokens(self):
        return self.tokens

    """
    Prints the list of tokens in an easily readable manner. For debugging.
    """
    def print_lexer(self):
        count = 0
        for token in self.tokens:
            print("<lexeme = \"" + ''.join(token.lexeme) + "\"; type = " + str(token.type) + "; lineNo. = " + str(token.line_num) + ";>")
            count += 1
