"""
add_os_classes creates symbol tables for all OS API classes
"""

import parser as ps
import lexer as lx

os_parsers = []

#add Math
ps.class_names.append("Math")
math_parser = ps.Parser(lx.Lexer())
table = math_parser.class_table
table.class_name = "Math"
table.new_sub("multiply", "function", "int")
table.insert("x", "int", "arg")
table.insert("y", "int", "arg")

table.new_sub("divide", "function", "int")
table.insert("x", "int", "arg")
table.insert("y", "int", "arg")

table.new_sub("min", "function", "int")
table.insert("x", "int", "arg")
table.insert("y", "int", "arg")

table.new_sub("max", "function", "int")
table.insert("x", "int", "arg")
table.insert("y", "int", "arg")

table.new_sub("sqrt", "function", "int")
table.insert("x", "int", "arg")

table.new_sub("abs", "function", "int")
table.insert("x", "int", "arg")

table.new_sub("init", "function", "void")

os_parsers.append(math_parser)

#add String
ps.class_names.append("String")
string_parser = ps.Parser(lx.Lexer())
table = string_parser.class_table
table.class_name = "String"
table.new_sub("new", "constructor", "String")
table.insert("maxLength", "int", "arg")

table.new_sub("dispose", "method", "int")

table.new_sub("length", "method", "int")

table.new_sub("charAt", "method", "char")
table.insert("j", "int", "arg")

table.new_sub("setCharAt", "method", "void")
table.insert("j", "int", "arg")
table.insert("c", "char", "arg")

table.new_sub("appendChar", "method", "String")
table.insert("c", "char", "arg")

table.new_sub("eraseLastChar", "method", "void")

table.new_sub("intValue", "method", "int")

table.new_sub("setInt", "method", "void")
table.insert("val", "int", "arg")

table.new_sub("backSpace", "function", "char")

table.new_sub("doubleQuote", "function", "char")

table.new_sub("newLine", "function", "char")

os_parsers.append(string_parser)

#add Array
ps.class_names.append("Array")
array_parser = ps.Parser(lx.Lexer())
table = array_parser.class_table
table.class_name = "Array"
table.new_sub("new", "function", "Array")
table.insert("size", "int", "arg")

table.new_sub("dispose", "method", "void")

os_parsers.append(array_parser)

#add Output
ps.class_names.append("Output")
out_parser = ps.Parser(lx.Lexer())
table = out_parser.class_table
table.class_name = "Output"
table.new_sub("moveCursor", "function", "void")
table.insert("i", "int", "arg")
table.insert("j", "int", "arg")

table.new_sub("printChar", "function", "void")
table.insert("c", "char", "arg")

table.new_sub("printString", "function", "void")
table.insert("s", "String", "arg")

table.new_sub("printInt", "function", "void")
table.insert("i", "int", "arg")

table.new_sub("println", "function", "void")

table.new_sub("backSpace", "function", "void")

os_parsers.append(out_parser)

#add Screen
ps.class_names.append("Screen")
screen_parser = ps.Parser(lx.Lexer())
table = screen_parser.class_table
table.class_name = "Screen"
table.new_sub("clearScreen", "function", "void")

table.new_sub("setColor", "function", "void")
table.insert("b", "boolean", "arg")

table.new_sub("drawPixel", "function", "void")
table.insert("x", "int", "arg")
table.insert("y", "int", "arg")

table.new_sub("drawLine", "function", "void")
table.insert("x1", "int", "arg")
table.insert("y1", "int", "arg")
table.insert("x2", "int", "arg")
table.insert("y2", "int", "arg")

table.new_sub("drawRectangle", "function", "void")
table.insert("x1", "int", "arg")
table.insert("y1", "int", "arg")
table.insert("x2", "int", "arg")
table.insert("y2", "int", "arg")

table.new_sub("drawCircle", "function", "void")
table.insert("x", "int", "arg")
table.insert("y", "int", "arg")
table.insert("r", "int", "arg")

os_parsers.append(screen_parser)

#add Keyboard
ps.class_names.append("Keyboard")
key_parser = ps.Parser(lx.Lexer())
table = key_parser.class_table
table.class_name = "Keyboard"
table.new_sub("keyPressed", "function", "char")

table.new_sub("readChar", "function", "char")

table.new_sub("readLine", "function", "String")
table.insert("message", "String", "arg")

table.new_sub("readInt", "function", "int")
table.insert("message", "String", "arg")

os_parsers.append(key_parser)

#add Memory
ps.class_names.append("Memory")
mem_parser = ps.Parser(lx.Lexer())
table = mem_parser.class_table
table.class_name = "Memory"
table.new_sub("peek", "function", "int")
table.insert("address", "int", "arg")

table.new_sub("poke", "function", "void")
table.insert("address", "int", "arg")
table.insert("value", "int", "arg")

table.new_sub("alloc", "function", "Array")
table.insert("size", "int", "arg")

table.new_sub("deAlloc", "function", "void")
table.insert("o", "Array", "arg")

os_parsers.append(mem_parser)

#add Sys
ps.class_names.append("Sys")
sys_parser = ps.Parser(lx.Lexer())
table = sys_parser.class_table
table.class_name = "Sys"
table.new_sub("halt", "function", "void")

table.new_sub("error", "function", "void")
table.insert("errorCode", "int", "arg")

table.new_sub("wait", "function", "void")
table.insert("duration", "int", "arg")

os_parsers.append(sys_parser)
