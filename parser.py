"""
A recursive descent parser
"""
import lexer
import symbol_table as st
import vm_writer as vm

class_names = []

"""
Parser is a class definition of a recursive descent parser with a procedure for
all valid JACK grammar statements. Parser takes a Lexer to read tokens from.
"""
class Parser():

    def __init__(self, tokens):
        self.class_table = st.SymbolTable()
        self.vm_writer = None
        self.id_counter = 0
        self.locals_counter = 0
        self.func_written = False
        self.temp_counter = 0
        self.tokens = tokens
        self.method_trackers = []
        self.exp_trackers = []
        if tokens.peek_next_token():
            self.class_table.class_name = tokens.filename
            self.class_declar()

    #parses class declaration
    def class_declar(self):
        if self.tokens.peek_next_token().lexeme != "class":
            self.error_print(
                    self.tokens.peek_next_token(),
                    "', the file should start with a class declaration"
            )

        self.tokens.get_next_token()
        if self.tokens.peek_next_token().type != "identifier":
            self.error_print(
                    self.tokens.peek_next_token(),
                    "', the class must be given a valid identifier"
            )

        temp_token = self.tokens.get_next_token()
        if self.tokens.filename != temp_token.lexeme:
            self.error_print(
                    temp_token,
                    "', the class name must be the same as the filename"
            )

        #check class name is unused
        if temp_token.lexeme in class_names:
            self.error_print(temp_token, "', class name already in use.")
        class_names.append(temp_token.lexeme)

        #setup vmwriter
        self.vm_writer = vm.VMWriter(temp_token.lexeme, self.tokens.dir)

        if self.tokens.peek_next_token().lexeme != '{':
            self.error_print(
                    self.tokens.peek_next_token(),
                    "', no open brace after class declaration"
            )
        self.tokens.get_next_token()

        while self.tokens.peek_next_token().lexeme != '}':
            self.member_declar(self.tokens.get_next_token())

        if self.tokens.peek_next_token().lexeme != '}':
            self.error_print(
                    self.tokens.peek_next_token(),
                    "', no closing brace after class member declaration."
            )
        self.tokens.get_next_token()


    #parses member declaration
    def member_declar(self, cur_token):
        if cur_token.lexeme == "static" or cur_token.lexeme == "field":
            self.class_var_declar(cur_token)
        elif (
                cur_token.lexeme == "constructor"
                or cur_token.lexeme == "function"
                or cur_token.lexeme == "method"
        ):
            self.subroutine_declar(cur_token)
        else:
            self.error_print(
                            cur_token,
                            "', a keyword (static, field, constructor, function, method) is expected here."
            )


    #parses class variable declaration
    def class_var_declar(self, cur_token):
        var_kind = cur_token.lexeme
        var_type = self.tokens.peek_next_token().lexeme

        self.type(self.tokens.get_next_token())  # check followed by valid type
        cur_token = self.tokens.get_next_token()

        if cur_token.type != "identifier":  # if not identifier then error
            self.error_print(cur_token, "', an identifier is expected here.")
        else:  # else check for repeating identifiers or semicolon

            #check if identifier is already in use
            if self.class_table.look_up(cur_token.lexeme):
                self.error_print(cur_token, "', identifier already declared in this scope.")

            #insert variable to symbol table
            var_name = cur_token.lexeme
            self.class_table.insert(var_name, var_type, var_kind)

            self.repeat_identifier(var_type, var_kind)

            if self.tokens.get_next_token().lexeme == ';':
                pass
            else:
                self.error_print(self.tokens.peek_next_token(), "', ';' expected.")


    #parses type
    def type(self, cur_token):
        if (
            cur_token.lexeme == "int"
            or cur_token.lexeme == "char"
            or cur_token.lexeme == "boolean"
            or cur_token.type == "identifier"
        ):
            pass
        else:
            self.error_print(cur_token, "', a variable type is expected here.")


    #parses subroutine declaration
    def subroutine_declar(self, cur_token):
        sub_type = cur_token.lexeme
        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme == "void":  # check followed by void keyword or a valid type
            pass
        else:
            self.type(cur_token)

        sub_kind = cur_token.lexeme

        cur_token = self.tokens.get_next_token()
        if cur_token.type != "identifier":
            self.error_print(cur_token, "', an identifier is expected here.")

        #check if identifier is already in use
        if self.class_table.look_up_sub(cur_token.lexeme):
            self.error_print(cur_token, "', identifier already declared in this scope.")

        #insert new sub procedure into symbol table
        sub_id = cur_token.lexeme
        self.class_table.new_sub(cur_token.lexeme, sub_type, sub_kind)

        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme != '(':
            self.error_print(cur_token, "', an open bracket is expected here for a list of parameters.")

        if (self.tokens.peek_next_token().lexeme != ")"):
            self.param_list(self.tokens.get_next_token())

        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme != ')':
            self.error_print(cur_token, "', a close bracket is expected here for the list of parameters.")

        #parse the subroutine body
        self.subroutine_body(self.tokens.get_next_token(), sub_id, sub_type)


    #parses parameter lists
    def param_list(self, cur_token):
        var_type = cur_token.lexeme
        var_kind = "arg"
        self.type(cur_token)
        if self.tokens.peek_next_token().type != "identifier":
            self.error_print(self.tokens.peek_next_token(), "', an identifier is expected here.")

        #insert the variable into the symbol table
        self.class_table.insert(self.tokens.peek_next_token().lexeme, var_type, var_kind)

        self.tokens.get_next_token()

        #handle repeating variable declarations
        if self.tokens.peek_next_token().lexeme == ",":
            while self.tokens.peek_next_token().lexeme == ",":
                self.tokens.get_next_token()
                var_type = self.tokens.peek_next_token().lexeme
                self.type(self.tokens.get_next_token())

                if self.tokens.peek_next_token().type != "identifier":
                    self.error_print(cur_token, "', an identifier is expected here.")

                #insert the variable into the symbol table
                self.class_table.insert(
                                        self.tokens.peek_next_token().lexeme,
                                        var_type,
                                        var_kind
                )

                self.tokens.get_next_token()
                if self.tokens.peek_next_token().lexeme == ",":
                    pass
                elif self.tokens.peek_next_token().lexeme == ")":
                    break


    #parses subroutine bodies
    def subroutine_body(self, cur_token, sub_id, sub_type = None):
        sub_ret_type = self.class_table.sub_ret_type(sub_id)
        stat_sequence = []
        if cur_token.lexeme != '{':
            self.error_print(cur_token, "', an open brace is expected before a subroutine body.")

        while self.tokens.peek_next_token().lexeme != "}":
            stat_sequence.append(self.statement(sub_id, sub_type))

        #used to decide when to print func definition to .vm
        self.func_written = False
        #check return statemnt present
        returned_val = False
        return_line = None
        #check the body declares a return for all code paths
        for stat in stat_sequence:
            if stat["stat_type"] == "return":
                returned_val = True
                return_line = stat["line_token"]
                break
            elif stat["stat_type"] == "if" and stat["complete_ret"] == True:
                returned_val = True
                return_line = stat["line_token"]
                break

        #check that a return statement was defined
        if returned_val == False:
            self.error_print(self.tokens.peek_next_token(), "', sub procedure has no return statement.")

        #check there is no code past the "absolute return", if there is then issue a warning
        if return_line:
            if (
                returned_val
                and return_line.line_num
                < stat_sequence[-1]["line_token"].line_num
            ):
                self.error_print(
                                return_line,
                                "', return statement(s) result in unreachable code near this point.",
                                True
                )

        #check that all variable declarations start at the beginning of the func
        declar_end = False
        for stat in stat_sequence:
            if stat["stat_type"] != "var":
                declar_end = True
            if declar_end and stat["stat_type"] == "var":
                self.error_print(
                                cur_token,
                                "', in " + sub_id
                                + " variable declaration must occur at the start of the function body."
                )

        self.tokens.get_next_token()

    #writes the .vm code for the initial .vm code for function definition
    def write_vm_func(self, sub_id, sub_type):
        if sub_type and self.func_written == False:
            #write function to .vm file
            self.vm_writer.write_func(
                                    self.class_table.class_name
                                    + "." + sub_id, self.locals_counter
            )
            if sub_type == "method":
                self.vm_writer.write_push("argument", 0)
                self.vm_writer.write_pop("pointer", 0)
            elif sub_type == "constructor":
                self.vm_writer.write_push("constant", self.class_table.get_n_field_vars())
                self.vm_writer.write_call("Memory.alloc", str(1))
                self.vm_writer.write_pop("pointer", 0)

            self.func_written = True
        self.locals_counter = 0
        sub_type = None

    #determines type of statement to be parsed and calls relevant function
    def statement(self, sub_id, sub_type = None):
        stat_ret = {}
        if self.tokens.peek_next_token().lexeme == "var":
            self.locals_counter += 1
            stat_ret = {
                "stat_type" : "var",
                "line_token" :  self.tokens.peek_next_token()
            }
            self.var_declar_statement(self.tokens.get_next_token())
        elif self.tokens.peek_next_token().lexeme == "let":
            self.write_vm_func(sub_id, sub_type)
            stat_ret = {
                "stat_type" : "let",
                "line_token" :  self.tokens.peek_next_token()
            }
            self.let_statement(self.tokens.get_next_token())
        elif self.tokens.peek_next_token().lexeme == "if":
            self.write_vm_func(sub_id, sub_type)
            stat_ret = {
                "stat_type" : "if",
                "line_token" : self.tokens.peek_next_token(),
                "complete_ret" : self.if_statement(self.tokens.get_next_token(), sub_id)
            }

        elif self.tokens.peek_next_token().lexeme == "while":
            self.write_vm_func(sub_id, sub_type)
            stat_ret = {
                "stat_type" : "while",
                "line_token" :  self.tokens.peek_next_token(),
                "complete_ret" : self.while_statement(self.tokens.get_next_token(), sub_id)
            }
        elif self.tokens.peek_next_token().lexeme == "do":
            self.write_vm_func(sub_id, sub_type)
            stat_ret = {
                "stat_type" : "do",
                "line_token" :  self.tokens.peek_next_token()
            }
            self.do_statement(self.tokens.get_next_token())
        elif self.tokens.peek_next_token().lexeme == "return":
            self.write_vm_func(sub_id, sub_type)
            stat_ret = {
                "stat_type" : "return",
                "line_token" :  self.tokens.peek_next_token()
            }
            self.return_statement(self.tokens.get_next_token(), sub_id)
        else:
            self.error_print(self.tokens.peek_next_token(), "', invalid syntax for a statement.")

        return stat_ret



    #parses variable declaration statements
    def var_declar_statement(self, cur_token):
        var_kind = "var"

        if cur_token.lexeme != "var":
            self.error_print(cur_token, "', 'var' expected in variable declaration.")

        cur_token = self.tokens.get_next_token()
        self.type(cur_token)
        var_type = cur_token.lexeme
        cur_token = self.tokens.get_next_token()
        if cur_token.type != "identifier":
            self.error_print(cur_token, "', identifier expected.")

        #check if identifier is already in use
        if self.class_table.check_used_sub(cur_token.lexeme) == True:
            self.error_print(cur_token, "', identifier already declared in this scope.")

        #insert variable into symbol table
        self.class_table.insert(cur_token.lexeme, var_type, var_kind)
        self.repeat_identifier(var_type, var_kind)

        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme != ';':
            self.error_print(cur_token, "', ';' expected at end of statement.")


    #parses let statements
    def let_statement(self, cur_token):
        if self.tokens.peek_next_token().type != "identifier":
            self.error_print(self.tokens.peek_next_token(), "', identifier expected.")

        #check identifier has been previously declared
        if self.class_table.look_up(self.tokens.peek_next_token().lexeme) == False:
            self.error_print(self.tokens.peek_next_token(), "', variable not declared.")

        var_index = self.class_table.get_var_offset(self.tokens.peek_next_token().lexeme)
        var_segment = self.class_table.get_var_segment(self.tokens.peek_next_token().lexeme)
        #set associated symbols 'init' field to true
        self.class_table.set_var_initialised(self.tokens.peek_next_token().lexeme)

        #gets the type of the variable fromt the class table
        lhs_type = self.class_table.look_up_var_type(self.tokens.peek_next_token().lexeme)
        rhs_type = ""

        arr = False
        self.tokens.get_next_token()
        if self.tokens.peek_next_token().lexeme == '[':
            self.tokens.get_next_token()
            index_type = self.expression(self.tokens.get_next_token())
            self.vm_writer.write_push(var_segment, var_index)
            self.vm_writer.write_arith("add")
            arr = True
            #check types are compatible
            if index_type in ["int", "char", "boolean"]:
                #if the types are not the same then throw an error (or warning for char = int)
                if index_type != "int":
                    self.error_print(
                                    self.tokens.peek_next_token(),
                                    "array index did not evaluate to an integer but to"
                                    + index_type,
                                    True
                    )
            #if the expression type returned is a method/functionTracker then add an
            #expression tracker to the MethodTracker to check the return type is valid
            elif isinstance(index_type, MethodTracker) or isinstance(index_type, FunctionTracker):
                ExpressionTracker(
                                cur_token,
                                "', the right hand side of the expression does not return the same type as the left.",
                                lhs_type,
                                index_type,
                                "arr_index"
                )
            if self.tokens.peek_next_token().lexeme != ']':
                self.error_print(
                                self.tokens.peek_next_token(),
                                "', closing square bracket expected after expression."
                )

            self.tokens.get_next_token()

        if self.tokens.peek_next_token().lexeme != '=':
            self.error_print(self.tokens.peek_next_token(), "', '=' expected.")

        self.tokens.get_next_token()
        #gets type of the rhs expression
        rhs_type = self.expression(self.tokens.get_next_token())
        #write to .vm so that result of expression is popped to variable
        if not arr:
            self.vm_writer.write_pop(var_segment, var_index)
        else:
            self.vm_writer.write_pop("temp", 0)
            self.vm_writer.write_pop("pointer", 1)
            self.vm_writer.write_push("temp", 0)
            self.vm_writer.write_pop("that", 0)
        #if the rhs type is an object reference then errors in type matching
        #are handled in code generation
        if rhs_type == "boolean" or rhs_type == "char" or rhs_type == "int":
            #if the types are not the same then throw an error
            #(or warning for primitives)
            if lhs_type != rhs_type and lhs_type != "Array":
                self.error_print(
                                self.tokens.peek_next_token(),
                                "type " + lhs_type + " being assigned to type "
                                + rhs_type,
                                True
                )
        elif rhs_type == "null":
            pass
        elif isinstance(rhs_type, MethodTracker) or isinstance(rhs_type, FunctionTracker):
            ExpressionTracker(
                            cur_token,
                            "', the right hand side of the expression does not return the same type as the left.",
                            lhs_type,
                            rhs_type
            )
        elif lhs_type == "Array" or rhs_type == "Array":
            pass
        elif lhs_type == "String" and rhs_type == "stringliteral":
            pass
        elif rhs_type != lhs_type:
            self.error_print(
                            self.tokens.get_next_token(),
                            "', type " + lhs_type +
                            " cannot be assigned to type "
                            + rhs_type
            )

        if self.tokens.peek_next_token().lexeme != ';':
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', ';' expected at end of statement."
            )

        self.tokens.get_next_token()




    """
    parses if (+else) statements. Returns True if there is a return in all code paths.
    """
    def if_statement(self, cur_token, sub_id):
        if cur_token.lexeme != "if":
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', if keyword expected at start of if statement."
            )

        if self.tokens.peek_next_token().lexeme != '(':
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', open bracket expected before if statement condition."
            )

        self.tokens.get_next_token()
        self.expression(self.tokens.get_next_token())

        #write if-goto statement in .vm
        self.id_counter += 1
        label = "if_true" + str(self.id_counter)
        e_label = "if_true_end" + str(self.id_counter)
        self.vm_writer.write_if(label)
        f_label = "if_false" + str(self.id_counter)
        self.vm_writer.write_goto(f_label)
        self.vm_writer.write_label(label)

        if self.tokens.peek_next_token().lexeme != ')':
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', close bracket expected after if statement condition."
            )

        self.tokens.get_next_token()
        if self.tokens.peek_next_token().lexeme != '{':
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', open bracket expected before if statement body."
            )

        self.tokens.get_next_token()
        stat_sequence = []
        while self.tokens.peek_next_token().lexeme != '}':
            stat_sequence.append(self.statement(sub_id))
        self.tokens.get_next_token()

        #check no unreachable code
        #check return statemnt present
        returned_val = False
        return_line = None
        for stat in stat_sequence:
            if stat["stat_type"] == "return":
                returned_val = True
                return_line = stat["line_token"]
            elif(
                stat["stat_type"] == "if"
                and stat["complete_ret"] == True
                or stat["stat_type"] == "while"
                and stat["complete_ret"] == True
            ):
                returned_val = True
                return_line = stat["line_token"]

        if returned_val and stat_sequence[-1]["stat_type"] != "return":
            self.error_print(
                            return_line,
                            "', return statement(s) result in unreachable code near this point.",
                            True
            )

        # if there is no else statement then end the function, else continue for else body
        if self.tokens.peek_next_token().lexeme != "else":
            self.vm_writer.write_label(f_label)
            return

        #write .vm labels for if statement
        self.vm_writer.write_goto(e_label)
        self.vm_writer.write_label(f_label)

        # only run if there is an else body in the if statement
        self.tokens.get_next_token()
        if self.tokens.peek_next_token().lexeme != '{':
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', open brace expected before else body."
            )

        self.tokens.get_next_token()
        stat_sequence_2 = []
        while self.tokens.peek_next_token().lexeme != '}':
            stat_sequence_2.append(self.statement(sub_id))
        self.tokens.get_next_token()


        #check no unreachable code
        #check return statemnt present
        returned_val_2 = False
        return_line = None
        for stat in stat_sequence_2:
            if stat["stat_type"] == "return":
                returned_val_2 = True
                return_line = stat["line_token"]
            elif(
                stat["stat_type"] == "if"
                and stat["complete_ret"] == True
                or stat["stat_type"] == "while"
                and stat["complete_ret"] == True
            ):
                returned_val_2 = True
                return_line = stat["line_token"]

        if returned_val_2 and stat_sequence_2[-1]["stat_type"] != "return":
            self.error_print(
                            return_line["line_token"],
                            "', return statement(s) result in unreachable code near this point.",
                            True
            )

        self.vm_writer.write_label(e_label)

        if returned_val and returned_val_2:
            return True
        else:
            return False


    #parses while statements. Returns true if there is a return statement within
    def while_statement(self, cur_token, sub_id):
        if cur_token.lexeme != "while":
            self.error_print(cur_token, "', while keyword expected at start of while statement.")

        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme != '(':
            self.error_print(cur_token, "', open bracket expected.")

        #write .vm labels for while loop
        self.id_counter += 1
        stlabel = "loop" + str(self.id_counter)
        elabel = "loopend" + str(self.id_counter)
        self.vm_writer.write_label(stlabel)
        self.expression(self.tokens.get_next_token())
        self.vm_writer.write_arith("not")
        self.vm_writer.write_if(elabel)

        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme != ')':
            self.error_print(cur_token, "', close bracket expected.")

        cur_token = self.tokens.get_next_token()
        if cur_token.lexeme != '{':
            self.error_print(cur_token, "', open brace expected.")

        stat_sequence = []
        while self.tokens.peek_next_token().lexeme != '}':
            stat_sequence.append(self.statement(sub_id))

        #write .vm labels for while loop
        self.vm_writer.write_goto(stlabel)
        self.vm_writer.write_label(elabel)

        #check for "absolute return"
        returned_val = False
        ret_line = None
        for stat in stat_sequence:
            if stat["stat_type"] == "return":
                returned_val = True
                ret_line = stat["line_token"]
            elif(
                stat["stat_type"] == "if"
                and stat["complete_ret"] == True
                or stat["stat_type"] == "while"
                and stat["complete_ret"] == True
            ):
                returned_val = True
                ret_line = stat["line_token"]

        self.tokens.get_next_token()

        if ret_line:
            if(
                returned_val
                and ret_line.line_num
                < stat_sequence[-1]["line_token"].line_num
            ):
                self.error_print(ret_line, "', return statement(s) result in unreachable code near this point.", True)

        return returned_val


    #parses do statements
    def do_statement(self, cur_token):
        if cur_token.lexeme != "do":
            self.error_print(cur_token, "', do keyword expected at start of do statement.")
        #parse subroutine call
        self.subroutine_call(self.tokens.get_next_token())
        if self.tokens.peek_next_token().lexeme != ";":
            self.error_print(self.tokens.peek_next_token(), "', ';' symbol expected at end of statement'.")

        self.tokens.get_next_token()


    #parses subroutine calls
    def subroutine_call(self, cur_token):
        if cur_token.type != "identifier":
            self.error_print(
                            cur_token,
                            "', identifier expected at start of a subroutine call."
            )
        temp_token = cur_token
        method_id = cur_token.lexeme
        method = False
        class_type = self.class_table.class_name
        #check if subroutine call is a method
        if self.tokens.peek_next_token().lexeme == '.':
            #check what class type the object is
            if cur_token.lexeme not in class_names:
                class_type = self.class_table.look_up_var_type(cur_token.lexeme)
            else:
                class_type = cur_token.lexeme
            self.tokens.get_next_token()

            if self.tokens.peek_next_token().type != "identifier":
                self.error_print(
                                self.tokens.peek_next_token(),
                                "', identifier expected after '.' symbol."
                )
            else:
                method = True
                temp_token = self.tokens.peek_next_token()
                method_id = temp_token.lexeme
                self.tokens.get_next_token()

        if self.tokens.peek_next_token().lexeme != '(':
            self.error_print(self.tokens.peek_next_token(), "', open bracket expected.")
        self.tokens.get_next_token()

        obj_type = None
        #write push to .vm file if subroutine call is a method (for object ref)
        #else set the obj_type to current class
        if method:
            obj_type = self.class_table.look_up_var_type(cur_token.lexeme)
            segment = self.class_table.get_var_segment(cur_token.lexeme)
            var_offset = self.class_table.get_var_offset(cur_token.lexeme)
            if segment and cur_token.lexeme not in class_names:
                self.vm_writer.write_push(
                                        segment,
                                        var_offset
                )

        else:
            obj_type = self.class_table.class_name

        #if obj_type is none then subroutine call is a function of self
        if obj_type == None:
            obj_type = cur_token.lexeme
            tracker = FunctionTracker(
                                    temp_token,
                                    obj_type,
                                    temp_token.lexeme,
                                    self.class_table
            )

        else:
            tracker = MethodTracker(
                                    temp_token,
                                    obj_type,
                                    temp_token.lexeme,
                                    self.class_table
            )

        self.method_trackers.append(tracker)

        if class_type == self.class_table.class_name:
            if method:
                #handles call to functions in current class from object of
                #current class
                if cur_token.lexeme == class_type:
                    self.vm_writer.write_push("pointer", 0)
            else:
                #simply a call to function of current class
                self.vm_writer.write_push("pointer", 0)

        #parse expression_list
        self.expression_list(temp_token.lexeme, tracker)
        #add NArgTracker to correct .vm line when all classes parsed and
        #the number of arguments is sure to be known
        self.vm_writer.add_narg_tracker(class_type, method_id)
        self.vm_writer.write_pop("temp", 0)
        if self.tokens.peek_next_token().lexeme != ')':
            self.error_print(self.tokens.peek_next_token(), "', close bracket expected.")

        self.tokens.get_next_token()

    #parses lists of expressions
    def expression_list(self, sub_id, tracker):
        if self.tokens.peek_next_token().lexeme != ')':
            var_type = self.expression(self.tokens.get_next_token())
            #appends a type to the trackers arg_type_sequence list
            #used for checking that methods are parsed the correct number and type
            #of args
            tracker.arg_type_sequence.append(var_type)

            #handles repeating expressions
            while self.tokens.peek_next_token().lexeme != ')':
                if self.tokens.peek_next_token().lexeme != ',':
                    self.error_print(
                                    self.tokens.peek_next_token(),
                                    "', unexpected token in expression list."
                    )

                self.tokens.get_next_token()

                #parse expression
                var_type = self.expression(self.tokens.get_next_token())
                #add arg type again
                tracker.arg_type_sequence.append(var_type)

    #parses return statements
    def return_statement(self, cur_token, sub_id):
        sub_ret_type = self.class_table.sub_ret_type(sub_id)
        if cur_token.lexeme != "return":
            self.error_print(cur_token, "', return keyword expected.")

        if self.tokens.peek_next_token().lexeme != ';':
            #parse expression
            expression_type = self.expression(self.tokens.get_next_token())
            primitive_ret_types = ["boolean", "char", "int", "null"]
            if expression_type in primitive_ret_types:
                #check that value being returned is of the correct type
                if expression_type != sub_ret_type:
                    self.error_print(
                                    cur_token,
                                    "', expected return type " + sub_ret_type
                                    + ", got " + expression_type,
                                    True
                    )

            elif isinstance(expression_type, MethodTracker) or isinstance(expression_type, FunctionTracker):
                ExpressionTracker(
                                cur_token,
                                "', incorrect type returned.",
                                sub_ret_type,
                                expression_type
                )

            else:
                self.exp_trackers.append(
                                ExpressionTracker(
                                                cur_token,
                                                "', expected return type "
                                                + sub_ret_type
                                                + ", got "
                                                + expression_type + ".",
                                                sub_ret_type,
                                                ret_type = expression_type
                                )
                )

            if self.tokens.peek_next_token().lexeme != ';':
                self.error_print(cur_token, "', ';' expected at end of statement.")

            self.tokens.get_next_token()
        elif self.tokens.peek_next_token().lexeme == ';' and sub_ret_type != "void":
            self.error_print(
                            self.tokens.peek_next_token(),
                            "', no value returned from non 'void' sub procedure."
            )

        elif self.tokens.peek_next_token().lexeme == ';' and sub_ret_type == "void":
            self.tokens.get_next_token()
        else:
            self.error_print(cur_token, "', ';' expected at end of statement.")


        #void functions always push 0
        if sub_ret_type == "void":
            self.vm_writer.write_push("constant", 0)
        #write function return to .vm
        self.vm_writer.write_ret()


    #parses expressions
    def expression(self, cur_token):
        expression_type = ""
        #parse relational expression
        expression_type = self.relational_expression(cur_token, expression_type)

        #parse cases where there are relational_expression separated by logical operators
        while(
            self.tokens.peek_next_token().lexeme == "&"
            or self.tokens.peek_next_token().lexeme == "|"
        ):
            cur_token = self.tokens.get_next_token()
            self.relational_expression(self.tokens.get_next_token(), expression_type)
            expression_type = "boolean"

            #handle .vm code gen
            if cur_token.lexeme == "&":
                self.vm_writer.write_arith("and")
            else:
                self.vm_writer.write_arith("or")
        return expression_type



    #parses relational expressions
    def relational_expression(self, cur_token, expression_type):
        expression_type = self.arithmetic_expression(cur_token, expression_type)
        #parse cases where there are arithmetic_expressions separated by arithmetic operators
        while(
            self.tokens.peek_next_token().lexeme == "="
            or self.tokens.peek_next_token().lexeme == "<"
            or self.tokens.peek_next_token().lexeme == ">"
        ):
            cur_token = self.tokens.peek_next_token()
            self.tokens.get_next_token()
            expression_type = self.arithmetic_expression(
                                                    self.tokens.get_next_token(),
                                                    expression_type
            )
            expression_type = "boolean"

            #handle .vm code gen
            if cur_token.lexeme == "=":
                self.vm_writer.write_arith("eq")
            elif cur_token.lexeme == "<":
                self.vm_writer.write_arith("lt")
            elif cur_token.lexeme == ">":
                self.vm_writer.write_arith("gt")

        return expression_type



    #parses arithmetic expressions
    def arithmetic_expression(self, cur_token, expression_type):
        expression_type = self.term(cur_token, expression_type)
        #parse cases where there are terms separated by arithmetic operators
        while(
            self.tokens.peek_next_token().lexeme == "+"
            or self.tokens.peek_next_token().lexeme == "-"
        ):
            cur_token = self.tokens.get_next_token()
            #track so that warnings are given where types incompatible
            if isinstance(expression_type, MethodTracker) or isinstance(expression_type, FunctionTracker):
                ExpressionTracker(
                                cur_token,
                                "', integer expected before '+' and '-' operators.",
                                "int",
                                expression_type
                )

            expression_type = self.factor(self.tokens.get_next_token(), expression_type)
            #track so that warnings are given where types incompatible
            if isinstance(expression_type, MethodTracker) or isinstance(expression_type, FunctionTracker):
                ExpressionTracker(
                                cur_token,
                                "', integer expected after '+' and '-' operators.",
                                "int",
                                expression_type
                )

            #handle .vm code gen
            if cur_token.lexeme == "+":
                self.vm_writer.write_arith("add")
            else:
                self.vm_writer.write_arith("sub")

        return expression_type


    #parses terms
    def term(self, cur_token, expression_type):
        expression_type = self.factor(cur_token, expression_type)
        #parse cases where there are factors separated by arithmetic operators
        while(
            self.tokens.peek_next_token().lexeme == "*"
            or self.tokens.peek_next_token().lexeme == "/"
        ):
            cur_token = self.tokens.get_next_token()
            #track so that warnings are given where types incompatible
            if isinstance(expression_type, MethodTracker) or isinstance(expression_type, FunctionTracker):
                ExpressionTracker(
                                cur_token,
                                "', integer expected before '*' and '/' operators.",
                                "int",
                                expression_type
                )

            expression_type = self.factor(self.tokens.get_next_token(), expression_type)
            #track so that warnings are given where types incompatible
            if isinstance(expression_type, MethodTracker) or isinstance(expression_type, FunctionTracker):
                ExpressionTracker(
                                cur_token,
                                "', integer expected after '*' and '/' operators.",
                                "int",
                                expression_type
                )

            #handle .vm code gen
            if cur_token.lexeme == "*":
                self.vm_writer.write_call("Math.multiply", 2)
            else:
                self.vm_writer.write_call("Math.divide", 2)

        return expression_type



    #parses factors
    def factor(self, cur_token, expression_type):
        temp_token = None
        #check if pushed operand must be negated or "not"ed
        if cur_token.lexeme == '-' or cur_token.lexeme == '~':
            temp_token = cur_token
            expression_type = self.operand(self.tokens.get_next_token(), expression_type)
        else:
            expression_type = self.operand(cur_token, expression_type)

        #handle .vm generation
        if temp_token:
            if temp_token.lexeme == '~':
                self.vm_writer.write_arith("not")
            else:
                self.vm_writer.write_arith("neg")

        return expression_type



    #parses operands
    def operand(self, cur_token, expression_type):
        op_token_type = ""
        if cur_token.type == "identifier":
            sub_id = cur_token.lexeme
            object_ref = cur_token
            op_token_type = self.class_table.look_up_var_type(cur_token.lexeme)

            #if the operand is a variable/method return of variable
            if self.tokens.peek_next_token().lexeme == ".":
                self.tokens.get_next_token()

                #check that next token is identifier
                if self.tokens.peek_next_token().type == "identifier":
                    sub_id = self.tokens.peek_next_token().lexeme
                    self.tokens.get_next_token()
                else:
                    self.error_print(
                                    self.tokens.peek_next_token(),
                                    "', identifier expected."
                    )

                #check if operand is referred to by [expression]
                if self.tokens.peek_next_token().lexeme == "[":
                    self.tokens.get_next_token()
                    index_type = self.expression(self.tokens.get_next_token())
                    if self.tokens.peek_next_token().lexeme != "]":
                        self.error_print(
                                        self.tokens.peek_next_token(),
                                        "', closing square bracket expected."
                        )

                    #check that the expression evaluates to an integer or Array
                    if index_type != "int" and index_type != "Array":
                        self.error_print(
                                        self.tokens.peek_next_token(),
                                        "', indices must evaluate to integers"
                        )

                    expression_type = "Array"
                    self.tokens.get_next_token()

                #handle expression list/expression (e.g. where operand is method call)
                if self.tokens.peek_next_token().lexeme == "(":
                    self.tokens.get_next_token()
                    class_type = ""
                    #create method/function tracker that is checked when all files have been parsed
                    if self.class_table.look_up_var_type(object_ref.lexeme) != None:
                        class_type = self.class_table.look_up_var_type(object_ref.lexeme)
                        tracker = MethodTracker(
                                                object_ref,
                                                class_type,
                                                sub_id,
                                                self.class_table,
                                                object_ref
                        )

                    else:
                        class_type = object_ref.lexeme
                        tracker = FunctionTracker(
                                                object_ref,
                                                object_ref.lexeme,
                                                sub_id,
                                                self.class_table
                        )

                    self.method_trackers.append(tracker)

                    #if it is a method call then push the reference to the object
                    var_segment = self.class_table.get_var_segment(cur_token.lexeme)
                    var_offset = self.class_table.get_var_offset(object_ref.lexeme)
                    if (
                        self.class_table.look_up(object_ref.lexeme)
                        and object_ref.lexeme not in class_names
                    ):
                        self.vm_writer.write_push(
                                                var_segment,
                                                var_offset
                        )

                    #parse expression
                    self.expression_list(sub_id, tracker)

                    #add tracker to correct the .vm file when n_args for the
                    #method is known
                    self.vm_writer.add_narg_tracker(class_type, sub_id)

                    if self.tokens.peek_next_token().lexeme != ")":
                        self.error_print(
                                        self.tokens.peek_next_token(),
                                        "', closing bracket expected."
                        )

                    expression_type = tracker
                    self.tokens.get_next_token()

            #if the operand is determined by index i.e. array element
            elif self.tokens.peek_next_token().lexeme == "[":

                var_segment = self.class_table.get_var_segment(cur_token.lexeme)
                var_offset = self.class_table.get_var_offset(cur_token.lexeme)
                self.tokens.get_next_token()
                index_type = self.expression(self.tokens.get_next_token())
                #handle .vm code for Array handling
                self.vm_writer.write_push(var_segment, var_offset)
                self.vm_writer.write_arith("add")
                self.vm_writer.write_pop("pointer", 1)
                self.vm_writer.write_push("that", 0)

                if index_type in ["int", "char", "boolean"]:
                    #if the types are not the same then print a warning
                    if index_type != "int":
                        self.error_print(
                                        self.tokens.peek_next_token(),
                                        "array index did not evaluate to an integer but to "
                                        + index_type,
                                        True
                        )

                elif (
                        isinstance(index_type, MethodTracker)
                        or isinstance(index_type, FunctionTracker)
                ):
                    ExpressionTracker(
                                    cur_token,
                                    "', the right hand side of the expression does not return the same type as the left.",
                                    lhs_type,
                                    index_type,
                                    "arr_index"
                    )

                if self.tokens.peek_next_token().lexeme != "]":
                    self.error_print(
                                    self.tokens.peek_next_token(),
                                    "', closing square bracket expected."
                    )

                #check that the expression evaluates to an integer or Array
                if index_type != "int" and index_type != "Array":
                    self.error_print(
                                    self.tokens.peek_next_token(),
                                    "', array index should evaluate to an integer",
                                    True
                    )

                expression_type = "Array"
                self.tokens.get_next_token()

            #if the operand is a function
            elif self.tokens.peek_next_token().lexeme == "(":
                #create a function tracker
                tracker = MethodTracker(
                                    cur_token,
                                    self.class_table.class_name,
                                    cur_token.lexeme,
                                    self.class_table
                )
                self.method_trackers.append(tracker)
                expression_type = tracker
                self.tokens.get_next_token()
                #parse expression list
                self.expression_list(cur_token.lexeme, tracker)

                #add tracker to correct the .vm file when n_args for the method is known
                self.vm_writer.add_narg_tracker(
                                            self.class_table.class_name,
                                            cur_token.lexeme
                )
                if self.tokens.peek_next_token().lexeme != ")":
                    self.error_print(
                            self.tokens.peek_next_token(),
                            "', closing bracket expected."
                    )

                self.tokens.get_next_token()

            #if the operand is an identifier
            else:
                #check variable has been previously declared
                if self.class_table.look_up(cur_token.lexeme) == False:
                    self.error_print(cur_token, "', variable not declared.")

                #check variable has been initialised
                if not self.class_table.check_initialised(cur_token.lexeme):
                    self.error_print(cur_token, "', variable not initialised.", True)

                #set the expression_type to the variables type
                expression_type = op_token_type
                var_segment = self.class_table.get_var_segment(cur_token.lexeme)
                var_offset = self.class_table.get_var_offset(cur_token.lexeme)
                #push the variable
                self.vm_writer.write_push(var_segment, var_offset)

        #if the operand is an expression/expressionlist
        elif cur_token.lexeme == "(":
            #parse expressio
            expression_type = self.expression(self.tokens.get_next_token())
            if (
                self.tokens.peek_next_token().lexeme != ")"
                and self.tokens.peek_next_token().lexeme != ","
            ):
                self.error_print(
                        self.tokens.peek_next_token(),
                        "', closing bracket expected."
                )
            self.tokens.get_next_token()

        elif cur_token.type == "constant":
            type = self.constant_to_type(cur_token.lexeme)
            #print warning for incompatible types
            self.check_type_maintained(cur_token, type, expression_type)
            expression_type = type

        elif cur_token.lexeme == "this":
            #push pointer to self
            expression_type = self.class_table.class_name
            self.vm_writer.write_push("pointer", 0)

        else:
            self.error_print(self.tokens.peek_next_token(), "', invalid operand.")

        return expression_type


    #handle cases where there are repeating identifiers seperated by commas
    def repeat_identifier(self, var_type, var_kind):
        multi = 0
        var_name = ""
        while True:
            if self.tokens.peek_next_token().lexeme == ',':
                self.tokens.get_next_token()
                if self.tokens.peek_next_token().type == "identifier":
                    var_name = self.tokens.get_next_token().lexeme
                    self.class_table.insert(var_name, var_type, var_kind)
                    if var_kind == "var":
                        self.locals_counter += 1
                    multi += 1
                    pass
                elif self.tokens.peek_next_token().lexeme == ";" and multi > 0:
                    break
                else:
                    if self.tokens.peek_next_token().lexeme == ";" and multi == 0:
                        self.error_print(
                                self.tokens.peek_next_token(),
                                "', multiple identifiers indicated."
                        )
                    else:
                        self.error_print(
                                self.tokens.peek_next_token(),
                                "', a variable type is expected here."
                        )
            elif self.tokens.peek_next_token().lexeme == ';':
                return
            else:
                self.error_print(self.tokens.peek_next_token(), "', ',' or ';' expected.")

    #checks that the token_type and expression_type are compatible
    #printing warnings where they are not
    def check_type_maintained(self, token, token_type, expression_type):
        primitives = ["char", "boolean", "int", "null"]
        if expression_type == "":
            expression_type = token_type
        elif token_type == expression_type:
            pass
        elif expression_type == "Array":
            pass
        elif (
                token_type in primitives
                and expression_type in primitives
        ):
            pass
        elif isinstance(expression_type, MethodTracker):
            ExpressionTracker(
                    token,
                    "', type not compatible with rest of expression.",
                    token_type,
                    expression_type
            )
        elif token_type == "null":
            pass
        else:
            self.error_print(token, "', type not compatible with rest of expression.")

    #determines what type a tokens lexeme is and writes .vm push commands
    def constant_to_type(self, lexeme):
        if lexeme == "false" or lexeme == "true":
            if lexeme == "false":
                self.vm_writer.write_push("constant", 0)
            else:
                self.vm_writer.write_push("constant", 1)
            return "boolean"
        elif lexeme == "null":
            self.vm_writer.write_push("constant", 0)
            return "null"
        elif self.is_int(lexeme):
            self.vm_writer.write_push("constant", int(lexeme))
            return "int"
        elif lexeme[0] == '"' and lexeme[-1] == '"':
            self.vm_writer.write_push("constant", len(lexeme[1:-1]))
            self.vm_writer.write_call("String.new", 1)
            for char in lexeme[1:-1]:
                self.vm_writer.write_push("constant", ord(char))
                self.vm_writer.write_call("String.appendChar", 2)
            return "stringliteral"
        else:
            self.vm_writer.write_push("constant", int(lexeme))
            return "char"

    #returns true if string is an integer constant and false otherwise
    def is_int(self, string):
        try:
            int(string)
            return True
        except ValueError:
            return False

    #loops through all trackers for the parser and calls there check() methods
    def check_trackers(self, tables):
        for tracker in self.method_trackers:
            tracker.check(tables)
        for exp_tracker in self.exp_trackers:
            exp_tracker.check()
        if self.vm_writer:
            self.vm_writer.correct_lines(tables)

    #prints a warning where warning == True and an error otherwise
    def error_print(self, token, custom_error_msg, warning = None):
        if not warning:
            print(
                    "Error (" + "Class " + self.class_table.class_name
                    + "): line " + str(token.line_num) + ", close to '"
                    + token.lexeme + custom_error_msg)
            exit(0)
        else:
            print(
                    "Warning (" + "Class " + self.class_table.class_name
                    + "): line " + str(token.line_num) + ", close to '"
                    + token.lexeme + custom_error_msg
            )

"""
Tracker defines a class with two methods one for printing errors, error_print
and one for checking type matches.

This class is implemented to provide common functionality to the two tracker
classes defined below.
"""
class Tracker():
    def __init__(self):
        pass

    #prints a warning where warning == True and an error otherwise
    def error_print(self, token, custom_error_msg, warning = None):
        if not warning:
            print(
                    "Error (" + "Class " + self.def_class
                    + "): line " + str(token.line_num) + ", close to '"
                    + token.lexeme + "'," + custom_error_msg
            )
            exit(0)
        else:
            print(
                    "Warning (" + "Class " + self.def_class
                    + "): line " + str(token.line_num) + ", close to '"
                    + token.lexeme + "', " + custom_error_msg
            )

    #compares two types and prints an appropriate warning or error
    def check_type_match(self, type1, type2, err_token):
        if type1 == "char" and type2 == "int":
            self.error_print(
                        err_token,
                        "char argument being passed int near line",
                        True
            )
        elif type1 == "int" and type2 == "char":
            self.error_print(
                            err_token,
                            "int argument being passed char near line",
                            True
            )
        elif type1 != "Array" and type2 == "Array":
            pass
        elif type1 == "Array" and type2 != "Array":
            pass
        elif type1 == "null" or type2 == "null":
            return True
        elif type1 == "String" and type2 == "stringliteral":
            pass
        elif type1 != type2:
            #adds an ExpressionTracker where a type is an instance of MethodTracker
            if isinstance(type2, MethodTracker) or isinstance(type2, FunctionTracker):
                ExpressionTracker(
                                err_token,
                                "', argument expected type " + str(type1),
                                type1,
                                type2
                )
            else:
                self.error_print(
                                err_token,
                                "', argument expected type " + type1
                                + " and got " + type2,
                                True
                )


"""
MethodTracker is a class that stores information about the use of a method and
provides the functionality to check whether this usage was valid.

Inherits from Tracker.
"""
class MethodTracker(Tracker):
    def __init__(self, err_token, object_type, method_id, table_ref, obj_ref = None):
        self.obj_ref = obj_ref
        self.object_type = object_type
        self.err_token = err_token
        self.method_id = method_id
        self.table_ref = table_ref
        self.arg_type_sequence = []
        self.expression_trackers = []
        self.def_class = class_names[-1]

    #checks that the use of the method was valid. Takes the list of symbol tables
    #for all classes in the program
    def check(self, tables):
        self.arg_type_sequence_expected = []
        obj_table = None

        #check object initialised
        if self.obj_ref:
            if (
                not self.table_ref.check_initialised(self.obj_ref.lexeme)
                and not self.obj_ref.lexeme in class_names
                ):
                self.error_print(self.obj_ref, "', object not initialised", True)
            if self.obj_ref.lexeme in class_names:
                self.object_type = self.obj_ref.lexeme

        #find symbol table corresponding to object type
        if self.object_type == self.table_ref.class_name:
            obj_table = self.table_ref
        else:
            for table in tables:
                if table.class_name == self.object_type:
                    obj_table = table
                    break;

        #check that the function exists
        if not obj_table.look_up(self.method_id):
            self.error_print(self.err_token, "', sub procedure not defined.")


        #check that the arguments passed where of the correct type and number
        arg_type_sequence_expected = obj_table.get_arg_type_sequence(self.method_id)

        if len(self.arg_type_sequence) != len(arg_type_sequence_expected):
            self.error_print(
                    self.err_token,
                    "', incorrect number of arguments passed. Expected "
                    + str(len(arg_type_sequence_expected))
                    + " and got "
                    + str(len(self.arg_type_sequence))
            )

        #check that the given argument sequence is correct
        count = 0
        for type in arg_type_sequence_expected:
            self.check_type_match(type, self.arg_type_sequence[count], self.err_token)
            count += 1

        #if method is a constructor set obj_ref to initialised
        if obj_table.sub_kind == "constructor":
            if self.obj_ref:
                obj_table.set_var_initialised(self.obj_ref.lexeme)

        #check expression trackers
        ret_type = obj_table.sub_ret_type(self.method_id)
        for tracker in self.expression_trackers:
            tracker.check(ret_type)

    #adds the given tracker to the list expression_trackers
    def add_exp_tracker(self, tracker):
        self.expression_trackers.append(tracker)

"""
FunctionTracker is a class that stores information about the use of a function and
provides the functionality to check whether this usage was valid.

Inherits from Tracker.
"""
class FunctionTracker(MethodTracker):
    def __init__(self, err_token, class_id, function_id, table_ref):
        self.err_token = err_token
        self.class_id = class_id
        self.function_id = function_id
        self.table_ref = table_ref
        self.arg_type_sequence = []
        self.expression_trackers = []
        self.def_class = class_names[-1]

    def check(self, tables):
        self.arg_type_sequence_expected = []
        class_table = None

        #get the symbol table for the class defining the function
        for table in tables:
            if table.class_name == self.class_id:
                class_table = table
                break;

        if class_table == None:
            self.error_print(self.err_token, "', " + self.class_id + " not defined.")

        #check that the function exists
        if not class_table.look_up(self.function_id):
            self.error_print(self.err_token, "', " + self.function_id + " not defined")


        #check that the arguments passed where of the correct type and number
        arg_type_sequence_expected = class_table.get_arg_type_sequence(self.function_id)
        if len(self.arg_type_sequence) != len(arg_type_sequence_expected):
            self.error_print(
                    self.err_token,
                    "', incorrect number of arguments passed. Expected "
                    + str(len(arg_type_sequence_expected)) + " got "
                    +  str(len(self.arg_type_sequence))
                    )

        #check that the given argument sequence is correct
        count = 0
        for type in arg_type_sequence_expected:
            self.check_type_match(type, self.arg_type_sequence[count], self.err_token)
            count += 1

        #if method is a constructor set obj_ref to initialised
        if class_table.sub_kind == "constructor":
            if self.obj_ref:
                class_table.set_var_initialised(self.obj_ref.lexeme)

        #check expression trackers
        ret_type = class_table.sub_ret_type(self.function_id)
        for tracker in self.expression_trackers:
            tracker.check(ret_type)

"""
ExpressionTracker is a class that stores information about an expression containing
elements unresolvable at the syntax stage

Inherits from Tracker.
"""
class ExpressionTracker(Tracker):
    def __init__(
                self, err_token, err_message, expected_type, method_tracker = None,
                ret_type = None, expr_type = None
                ):
        self.err_token = err_token
        self.err_message = err_message
        self.expected_type = expected_type
        self.ret_type = ret_type
        self.expr_type = expr_type
        self.def_class = class_names[-1]
        if method_tracker:
            method_tracker.add_exp_tracker(self)

    #checks that the previously unresolvable element returns a valid type
    def check(self, ret_type = None):
        if ret_type:
            if not self.allowed_casts(ret_type, self.expected_type, self.expr_type):
                if self.expected_type != ret_type:
                    self.error_print(self.err_token, self.err_message, True)
        elif self.ret_type:
            if not self.allowed_casts(self.ret_type, self.expected_type, self.expr_type):
                if self.expected_type != self.ret_type:
                    self.error_print(self.err_token, self.err_message, True)

    #prints appropriate warning messages for incorrect type matches
    def allowed_casts(self, type1, type2, expr_type):
        primitives = ["char", "int", "null"]

        if expr_type == "arr_index":
            self.error_print(
                            self.err_token,
                            "array index did not evaluate to an integer but to "
                            + type2,
                            True
            )
            return True
        if type1 == "char" and type2 == "int":
            self.error_print(
                            self.err_token,
                            "char argument being passed int ",
                            True
            )
            return True
        elif type1 == "int" and type2 == "char":
            self.error_print(
                            self.err_token,
                            "int argument being passed char ",
                            True
            )
            return True
        elif type1 == "null" or type2 == "null":
            return True
        elif type1 != "Array" and type2 == "Array":
            return True
        elif type1 == "Array" and type2 != "Array":
            return True
        elif type1 == "String" and type2 == "stringliteral":
            return True
        else:
            return False
