"""
Read the jack files in a given directory and compiles the code
"""

import sys
import os
import add_os_classes as osc
import lexer as lx
import parser as ps
import symbol_table as st

def main(argv):
    #check correct number of arguments provided
    if len(argv) != 2:
        print("Incorrect number of arguments.\nCorrect usage: jack_compiler.py Path/To/Directory")
        exit(0)
    files = []
    try:
        for file in os.listdir(argv[1]):
            if file.endswith(".jack"):
                files.append(argv[1]  + "/" + file)
        dir = argv[1] + '/'
    except FileNotFoundError:
        print("Directory does not exist.")
        exit(0)
        
    global_symbol_table = st.SymbolTable()

    parsers = [(ps.Parser(lx.Lexer(file, dir))) for file in files]
    for parser in osc.os_parsers:
        parsers.append(parser)

    tables = [(parser.class_table) for parser in parsers]
    for parser in parsers:
        parser.check_trackers(tables)



main(sys.argv)
